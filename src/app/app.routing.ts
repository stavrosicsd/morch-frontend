import { RouterModule, Routes } from '@angular/router';
import { ModuleWithProviders } from '@angular/core/src/metadata/ng_module';
import {LoginComponent} from './login/login.component';
import {AuthGuard} from './auth.guard';
import {ControlPanelComponent} from './control-panel/control-panel.component';

export const AppRoutes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'trees', canActivate: [AuthGuard], component: ControlPanelComponent }
];

export const ROUTING: ModuleWithProviders = RouterModule.forRoot(AppRoutes);
