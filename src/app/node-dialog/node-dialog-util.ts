import {Injectable} from '@angular/core';
import {NodeType, TemplateType, TreeNode} from '../model';

@Injectable()
export class NodeDialogUtil {

  isString(templateType: TemplateType): boolean {
    return templateType === TemplateType.STRING;
  }

  isEnum(templateType: TemplateType): boolean {
    return templateType === TemplateType.ENUM;
  }

  isConditional(templateType: TemplateType): boolean {
    return templateType === TemplateType.CONDITIONAL;
  }

  isMonitoring(node: TreeNode): boolean {
    return node.type === NodeType.MONITORING;
  }

  hasChildren(node: TreeNode): boolean {
    return node.children.length > 0;
  }

  hasAttackChildren(node: TreeNode): boolean {
    return this.hasChildren(node) && node.children[0].type === NodeType.ATTACK;
  }
}
