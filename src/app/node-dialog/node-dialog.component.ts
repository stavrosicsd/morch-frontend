import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {
  ConditionalParameterTemplate,
  EnumParameterTemplate,
  MonitoringRule,
  NodeType,
  Parameter,
  Specification,
  StringParameterTemplate,
  TemplateType,
  TreeNode
} from '../model';
import {AbstractControl, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {environment} from '../../environments/environment';
import {HttpClient} from '../../../node_modules/@angular/common/http';
import {NodeDialogUtil} from './node-dialog-util';

@Component({
  selector: 'app-node-dialog',
  templateUrl: './node-dialog.component.html',
  styleUrls: ['./node-dialog.component.scss'],
  providers: [NodeDialogUtil]
})
export class NodeDialogComponent implements OnInit {

  form: FormGroup;
  ruleFormGroup: Map<string, FormGroup> = new Map<string, FormGroup>();
  labelMaxLength = 50;
  hasAttackChildren: boolean;
  isMonitoring: boolean;
  assets: string[] = [];
  assetToTool: Map<string, string[]> = new Map<string, string[]>();
  services: string[];
  selectedAsset: string = undefined;
  selectedTool: string = undefined;
  assetToolToRuleParameters: Map<string, Parameter[]> = new Map<string, Parameter[]>();
  multipleAllowedParameters: string[] = [];

  constructor(private formBuilder: FormBuilder,
              private httpClient: HttpClient,
              private dialogRef: MatDialogRef<NodeDialogComponent>,
              @Inject(MAT_DIALOG_DATA) private data: TreeNode,
              public util: NodeDialogUtil) {}


  ngOnInit() {
    this.hasAttackChildren = this.util.hasAttackChildren(this.data);
    this.isMonitoring = this.data.type === NodeType.MONITORING;
    this.initializeForm();
  }

  private initializeForm() {
    this.form = this.formBuilder.group({
      label: [this.data.label, [Validators.required, Validators.maxLength(this.labelMaxLength)]]
    });
    if (this.hasAttackChildren) {
      this.form.addControl('refinement', new FormControl(this.data.refinement, Validators.required));
    }
    if (this.isMonitoring) {
      if (this.data.rule === undefined) {
        this.data.rule = new MonitoringRule();
      }
      this.selectedAsset = this.data.rule.asset;
      this.selectedTool = this.data.rule.tool;
      this.setServicesForTool();
      this.initializeMonitoringConfigurationRuleParameters();
    }
  }

  private initializeMonitoringConfigurationRuleParameters() {
    const ruleControlGroup = this.formBuilder.group({
      asset: [this.data.rule.asset, [Validators.required]],
      tool: [this.data.rule.tool, [Validators.required]],
      service: [this.data.rule.service, [Validators.required]],
      parameters: [ this.data.rule.parameters, this.formBuilder.group({})],
    });
    this.form.addControl('rule', ruleControlGroup);
    this.httpClient.get(environment.apiUrl + '/specification').subscribe((specification: Specification[]) => {
      for (const spec of specification) {
        this.ruleFormGroup[this.assetToolKey(spec.asset, spec.tool)] = this.getRuleParametersFormControls(spec.parameters, this.data.rule.parameters);
        this.initializeViewVariables(spec);
      }
      if (this.selectedAsset && this.selectedTool) {
        this.form.setControl('parameters', new FormGroup(this.ruleFormGroup[this.assetToolKey(this.selectedAsset, this.selectedTool)]));
      }
    });
  }

  initializeViewVariables(spec: Specification) {
    this.assets.push(spec.asset);
    if (this.assetToTool[spec.asset] === undefined) {
      this.assetToTool[spec.asset] = [];
    }
    this.assetToTool[spec.asset].push(spec.tool);
    this.assetToolToRuleParameters[this.assetToolKey(spec.asset, spec.tool)] = spec.parameters;
  }

  assetToolKey(asset: string, tool: string) {
    return asset + '-' + tool;
  }

  getRuleParametersFormControls(parameters: Parameter[], initParameterValues: Map<string, string>): any {
    const dynamicControlsGroup: any = {};
    const sortedParameters: Parameter[] = parameters.sort(function(a, b) {return a.position - b.position; });
    for (const parameter of sortedParameters) {
      const validators = [];
      if (parameter.multipleAllowed && parameter.name.indexOf('-') === -1) {
        this.multipleAllowedParameters.push(parameter.name);
        parameter.name += '-1';
      }
      if (parameter.required) {
        validators.push(Validators.required);
      }
      if (parameter.template.type === TemplateType.STRING) {
        validators.push(Validators.pattern((<StringParameterTemplate> parameter.template).regex));
        dynamicControlsGroup[parameter.name] = new FormControl(initParameterValues[parameter.name], validators);
      } else if (parameter.template.type === TemplateType.ENUM) {
        validators.push((control: AbstractControl) => {
          return (<EnumParameterTemplate> parameter.template).values.indexOf(control.value) !== -1 ? null : {'forbiddenValue': true};
        });
        dynamicControlsGroup[parameter.name] = new FormControl(initParameterValues[parameter.name], validators);
      } else if (parameter.template.type === TemplateType.CONDITIONAL) {
        this.appendConditionalParameterDynamicGroup(dynamicControlsGroup, parameter, initParameterValues);
      }
    }
    return dynamicControlsGroup;
  }

  appendConditionalParameterDynamicGroup(dynamicControlsGroup: any, parameter: Parameter, initParameterValues: Map<string, string>) {
    const keyValidator = [Validators.required, (control: AbstractControl) => {
      return (<ConditionalParameterTemplate> parameter.template).keys.indexOf(control.value) !== -1 ? null : {'forbiddenValue': true};
    }];
    const key = parameter.name + '-key';
    dynamicControlsGroup[key] = new FormControl(initParameterValues[key], keyValidator);

    const operatorValidator = [Validators.required, (control: AbstractControl) => {
      return (<ConditionalParameterTemplate> parameter.template).operators.indexOf(control.value) !== -1 ? null : {'forbiddenValue': true};
    }];
    const operator = parameter.name + '-operator';
    dynamicControlsGroup[operator] = new FormControl(initParameterValues[operator], operatorValidator);

    const value = parameter.name + '-value';
    const valueValidators = [Validators.required, Validators.pattern((<ConditionalParameterTemplate> parameter.template).value)];
    dynamicControlsGroup[value] = new FormControl(initParameterValues[value], valueValidators);
  }

  cancel() {
    this.dialogRef.close(this.data);
  }

  save() {
    if (this.form.valid) {
      if (this.isMonitoring) {
        this.data.rule = this.form.value.rule;
        this.data.rule.parameters = this.form.value.parameters;
      }
      this.dialogRef.close(this.form.value);
    }
  }

  assetChanged() {
    this.selectedTool = undefined;
    this.services = undefined;
  }

  prepareDynamicRuleParameters() {
    this.form.setControl('parameters', new FormGroup(this.ruleFormGroup[this.assetToolKey(this.selectedAsset, this.selectedTool)]));
    this.setServicesForTool();
  }

  setServicesForTool() {
    if (this.selectedTool) {
      this.httpClient.get(environment.apiUrl + '/tools/' + this.selectedTool + '/services').subscribe((services: string[]) => {
        this.services = services;
      });
    }
  }

  parameters() {
    return this.assetToolToRuleParameters[this.assetToolKey(this.selectedAsset, this.selectedTool)];
  }

  removeParameter(parameter: Parameter) {
    if (!parameter.name.endsWith('1')) {
      const assetToolToRuleParameters: Parameter[] = this.assetToolToRuleParameters[this.assetToolKey(this.selectedAsset, this.selectedTool)];
      assetToolToRuleParameters.splice(assetToolToRuleParameters.indexOf(parameter), 1);
      this.updateFormControls(assetToolToRuleParameters);
    }
  }

  createParameter(insertIndex: number, parameterOriginalName: string) {
    const assetToolToRuleParameters: Parameter[] = this.assetToolToRuleParameters[this.assetToolKey(this.selectedAsset, this.selectedTool)];
    let previousParameterLike: Parameter;
    for (let i = insertIndex - 1; i > 0; i--) {
      const currentParameter = assetToolToRuleParameters[i];
      if (currentParameter.name.startsWith(parameterOriginalName)) {
        if ((!this.util.isConditional(currentParameter.template.type) && this.form.value.parameters[currentParameter.name]) ||
          (this.util.isConditional(currentParameter.template.type) && this.form.value.parameters[currentParameter.name + '-key'] &&
            this.form.value.parameters[currentParameter.name + '-operator'] && this.form.value.parameters[currentParameter.name + '-value'])) {
          previousParameterLike = currentParameter;
          break;
        }
      }
    }
    if (previousParameterLike === undefined) {
      return;
    }

    const newParameter: Parameter = Object.assign({}, previousParameterLike);
    this.increaseParameterNameIndex(newParameter);
    newParameter.position = insertIndex + 1;
    assetToolToRuleParameters.splice(insertIndex, 0, newParameter);

    for (let i = insertIndex + 1; i < assetToolToRuleParameters.length; i++) {
      const currentParameter = assetToolToRuleParameters[i];
      if (currentParameter.name.startsWith(parameterOriginalName)) {
        this.increaseParameterNameIndex(assetToolToRuleParameters[i]);
      }
      assetToolToRuleParameters[i].position++;
    }
    this.updateFormControls(assetToolToRuleParameters);
  }

  updateFormControls(parameters: Parameter[]) {
    this.ruleFormGroup[this.assetToolKey(this.selectedAsset, this.selectedTool)] =
      this.getRuleParametersFormControls(parameters, this.form.value.parameters);
    this.form.setControl('parameters', new FormGroup(this.ruleFormGroup[this.assetToolKey(this.selectedAsset, this.selectedTool)]));
  }

  increaseParameterNameIndex(parameter: Parameter) {
    const parameterNameSplit = parameter.name.split('-');
    parameter.name = parameterNameSplit[0] + '-' + (+parameterNameSplit[1] + 1);
  }

  maxWidth(parameter: Parameter): number {
    const paramMaxLength = parameter.maxLength;
    if (paramMaxLength >= 0 && paramMaxLength < 5) {
      return paramMaxLength * 30;
    } else if (paramMaxLength >= 5 && paramMaxLength < 12) {
      return paramMaxLength * 12;
    } else if (paramMaxLength >= 12 && paramMaxLength <= 50) {
      return paramMaxLength * 8;
    }
  }
}
