import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {AuthService} from '../auth/auth.service';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  invalidCredentials = false;

  form: FormGroup;

  constructor(private authService: AuthService, private router: Router, private formBuilder: FormBuilder, private httpClient: HttpClient) { }

  ngOnInit() {
    this.form = this.formBuilder.group({
      username: ['', [Validators.required, this.noWhitespaceValidator]],
      password: ['', [Validators.required, Validators.minLength(8)]]
    });
  }

  public noWhitespaceValidator(control: FormControl) {
    const isWhitespace = (control.value || '').trim().length === 0;
    const isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true };
  }

  login() {
    if (this.form.valid) {
      const username = this.form.get('username').value;
      const password = this.form.get('password').value;
      this.httpClient.post(environment.apiUrl + '/auth', {'username': username, 'password': password}, {observe: 'response'}).subscribe(resp => {
        if (resp.status === 200) {
          this.invalidCredentials = false;
          this.authService.loggedIn = true;
          this.authService.role = 'ADMIN';
          this.router.navigate(['trees']);
        } else {
          this.invalidCredentials = true;
        }
      });

    } else {
      this.markFormGroupTouched(this.form);
    }
  }

  private markFormGroupTouched(formGroup: FormGroup) {
    (<any>Object).values(formGroup.controls).forEach(control => {
      control.markAsTouched();

      if (control.controls) {
        control.controls.forEach(c => this.markFormGroupTouched(c));
      }
    });
  }

}
