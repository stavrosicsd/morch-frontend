import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { RoutingModule } from './routing.module';
import { AppComponent } from './app.component';
import { NodeComponent } from './node/node.component';
import { TreeComponent } from './tree/tree.component';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  MatSlideToggleModule, MatCardModule, MatButtonModule, MatFormFieldModule, MatDialogModule,
  MatInputModule, MatSelectModule, MatOptionModule, MatIconModule, MatMenuModule, MatToolbarModule
} from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { ControlPanelComponent } from './control-panel/control-panel.component';
import { Ng2PanZoomModule } from 'ng2-panzoom';
import { NodeDialogComponent } from './node-dialog/node-dialog.component';
import {ContextMenuModule} from 'ngx-contextmenu';
import {UploadFileService} from './control-panel/upload-file.service';
import {NavbarComponent} from './navbar/navbar.component';
import {LoginComponent} from './login/login.component';
import {ROUTING} from './app.routing';
import {AuthGuard} from './auth.guard';
import {AuthService} from './auth/auth.service';
import {DragDropModule} from '@angular/cdk/drag-drop';
import { NgxSpinnerModule } from 'ngx-spinner';
import {Util} from './util';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    NavbarComponent,
    NodeComponent,
    TreeComponent,
    ControlPanelComponent,
    NodeDialogComponent
  ],
  imports: [
    BrowserModule,
    RoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatSlideToggleModule,
    MatCardModule,
    DragDropModule,
    MatButtonModule,
    MatFormFieldModule,
    MatDialogModule,
    MatInputModule,
    MatIconModule,
    MatMenuModule,
    MatButtonModule,
    MatToolbarModule,
    MatIconModule,
    FlexLayoutModule,
    PerfectScrollbarModule,
    FormsModule,
    Ng2PanZoomModule,
    ReactiveFormsModule,
    MatSelectModule,
    MatOptionModule,
    ROUTING,
    ContextMenuModule.forRoot(),
    NgxSpinnerModule
  ],
  providers: [AuthGuard, AuthService, UploadFileService, Util],
  entryComponents: [NodeDialogComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
