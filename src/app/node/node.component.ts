import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {MonitoringRule, newTreeNode, NodeType, Refinement, TreeNode} from '../model';
import {MatDialog, MatDialogRef} from '@angular/material';
import {NodeDialogComponent} from '../node-dialog/node-dialog.component';
import {ContextMenuComponent} from 'ngx-contextmenu';
import {NodeUtil} from './node-util';
import {Util} from '../util';

@Component({
  selector: 'node',
  templateUrl: './node.component.html',
  styleUrls: ['./node.component.scss'],
  providers: [NodeUtil]
})
export class NodeComponent implements OnInit {

  @Input() parent: TreeNode;
  @Input() node: TreeNode;
  @ViewChild(ContextMenuComponent) private basicMenu: ContextMenuComponent;
  private nodeDialogRef: MatDialogRef<NodeDialogComponent>;
  private dialogWidth = '800px';

  constructor(private dialog: MatDialog, public util: NodeUtil, private commonUtil: Util) { }

  ngOnInit() {
    if (this.util.isMonitoring(this.node) && this.node.rule === undefined) {
      this.node.rule = new MonitoringRule();
    }
    // if(this.parent == null) {
      // for (let i = 0; i < 64; i++) {
      //   this.node.children.push(this.node.children[1]);
      // }
    // }
    // if(this.util.isAttack(this.node) && this.parent != null && this.util.hasChildren(this.node)) {
      // for (let i = 65; i <= 128; i++) {
      //   const newNode = JSON.parse(JSON.stringify(this.node.children[0]));
      //   newNode.rule.service = 'sum_mariadb' + (1000000 + (i));
      //   this.node.children.push(newNode);
      // }
      // for (let i = 0; i < 2047; i++) {
      //   this.node.children.push(this.node.children[1]);
      // }
    // }
  }

  parameters(): string[] {
    let ruleParameters: string[] = [];
    for (const ruleParameter of Object.keys(this.node.rule)) {
      if (ruleParameter !== 'parameters') {
        //TODO: this is a hack: we need to change asset notion to event in the backend
        if(ruleParameter === 'asset') {
          ruleParameters.push('event: ' + this.node.rule[ruleParameter]);
        } else {
          ruleParameters.push(ruleParameter + ': ' + this.node.rule[ruleParameter]);
        }
      }
    }
    for (const ruleParameter of Object.keys(this.node.rule.parameters)) {
      if(ruleParameter != null) {
        ruleParameters.push(ruleParameter + ': ' + this.node.rule.parameters[ruleParameter]);
      }
    }
    return ruleParameters;
  }

  deleteNode(node: TreeNode) {
    for (let i = 0; i < this.parent.children.length; i++) {
      if (this.parent.children[i].label === node.label) {
        this.parent.children.splice(i, 1);
        break;
      }
    }
  }

  editNode(node: TreeNode): void {
    this.openDialog(node);
    this.nodeDialogRef.afterClosed().subscribe(result => {
      node.label = result.label;
      node.refinement = result.refinement;
    });
  }

  openDialog(node: TreeNode) {
    this.nodeDialogRef = this.dialog.open(NodeDialogComponent, {
      width: this.dialogWidth,
      data: node,
      disableClose: true
    });
  }

  addAttack(node: TreeNode) {
    this.addChild(node, NodeType.ATTACK);
  }

  addMonitoring(node: TreeNode) {
    this.addChild(node, NodeType.MONITORING);
  }

  addChild(parent: TreeNode, type: NodeType) {
    let child: TreeNode = newTreeNode(type);
    this.openDialog(child);
    this.nodeDialogRef.afterClosed().subscribe(result => {
      if (result !== child) {
        child.label = result.label;
        parent.children.push(child);
        if (this.util.hasMoreThanOneChildren(parent)) {
          parent.refinement = type === NodeType.ATTACK ? Refinement.OR : Refinement.AND;
        }
      }
    });
  }

  showEvidence(event) {
    event.stopPropagation();
    window.open(this.commonUtil.kibanaUrl(this.node.label),"_blank");
  }
}
