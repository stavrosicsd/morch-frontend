import {Injectable} from '@angular/core';
import {NodeType, TreeNode} from '../model';

@Injectable()
export class NodeUtil {
  isAttack(node: TreeNode): boolean {
    return node.type === NodeType.ATTACK;
  }

  isMonitoring(node: TreeNode): boolean {
    return node.type === NodeType.MONITORING;
  }

  hasChildren(node: TreeNode): boolean {
    return node.children.length > 0;
  }

  hasMoreThanOneChildren(node: TreeNode): boolean {
    return node.children.length > 1;
  }

  canBeMonitored(node: TreeNode) {
    return this.isAttack(node) && (!this.hasChildren(node) || this.isMonitored(node));
  }

  isNotMonitored(node: TreeNode): boolean {
    return !this.isMonitored(node);
  }

  isMonitored(node: TreeNode): boolean {
    return this.hasChildren(node) && node.children[0].type === NodeType.MONITORING;
  }
}
