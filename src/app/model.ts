export class Tree {
  constructor(id: string) {
    this.id = id;
    this.root = newTreeNode(NodeType.ATTACK);
  }

  id: string;
  active = true;
  root: TreeNode;
}

export class TreeNode {
  constructor(label: string, type: NodeType) {
    this.label = label;
    this.type = type;
  }

  label: string;
  type: NodeType;
  refinement: Refinement;
  rule?: MonitoringRule;
  children: TreeNode[] = [];
}

export class MonitoringRule {
  service: string;
  asset: string;
  tool: string;
  parameters: Map<string, string> = new Map<string, string>();
}

export class Specification {
  asset: string;
  tool: string;
  parameters: Parameter[];
}

export class Parameter {
  constructor(name: string, required: boolean, position: number, multipleAllowed, maxLength: number, template: Template) {
    this.name = name;
    this.required = required;
    this.position = position;
    this.multipleAllowed = multipleAllowed;
    this.maxLength = maxLength;
    this.template = template;
  }
  name: string;
  required: boolean;
  position: number;
  multipleAllowed: boolean;
  maxLength: number;
  template: Template;
}

export interface Template {
  type: TemplateType;
}

export enum TemplateType {
  STRING = 'string',
  ENUM = 'enum',
  CONDITIONAL = 'conditional',
  LIST = 'list'
}

export class StringParameterTemplate implements Template {
  type = TemplateType.STRING;
  regex: string;
}

export class EnumParameterTemplate implements Template {
  type = TemplateType.ENUM;
  values: string[];
}

export class ConditionalParameterTemplate implements Template {
  type = TemplateType.CONDITIONAL;
  keys: string[];
  operators: string[];
  value: string;
}

export class ListParameterTemplate implements Template {
  type = TemplateType.LIST;
  values: string;
}

export function newTree(id: string): Tree {
  return new Tree(id);
}

export function newTreeNode(type: NodeType): TreeNode {
  return new TreeNode('New', type);
}

export enum NodeType {
  ATTACK = 'attack',
  MONITORING = 'monitoring'
}

export enum Refinement {
  OR = 'OR',
  AND = 'AND'
}
