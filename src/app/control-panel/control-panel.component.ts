import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {HttpClient, HttpEventType, HttpResponse} from '../../../node_modules/@angular/common/http';
import {environment} from '../../environments/environment';
import {Tree, newTree} from '../model';
import {DomSanitizer} from '@angular/platform-browser';
import {UploadFileService} from './upload-file.service';
import {CdkDragDrop, moveItemInArray, transferArrayItem} from "@angular/cdk/drag-drop";
import { NgxSpinnerService } from 'ngx-spinner';
import {Util} from '../util';

@Component({
  selector: 'control-panel',
  templateUrl: './control-panel.component.html',
  styleUrls: ['./control-panel.component.scss']
})
export class ControlPanelComponent implements OnInit {
  trees: Tree[];
  tree: Tree;
  fileUrl;
  createStyle = { 'width.px': 50, 'margin.px.top': -3};
  importStyle = { 'width.px': 50, 'height.px': 26, 'margin.px.top': -5};
  selectedFiles: FileList;
  currentFileUpload: File;
  progress: { percentage: number } = { percentage: 0 };
  @ViewChild('mySelectedFile')
  mySelectedFile: any;
  startTime: number;

  constructor(private httpClient: HttpClient, private sanitizer: DomSanitizer, private uploadService: UploadFileService,
              private spinner: NgxSpinnerService, private commonUtil: Util) {}

  ngOnInit(): void {
    this.reload();
  }

  reload() {
    this.httpClient.get(environment.apiUrl + '/amts').subscribe((result : Tree[])=>{
      this.trees = result;
      this.tree = this.trees[0];
      this.mySelectedFile.nativeElement.value = '';
    });
  }

  selectTree(index: number) {
    this.tree = this.trees[index];
  }

  createTree() {
    const maxId = this.trees.length > 0 ? Math.max.apply(Math, this.trees.map(function(o) { return o.id; })) : 1;
    this.trees.unshift(newTree(String(maxId + 1)));
    this.trees = Object.assign([], this.trees);
    this.tree = this.trees[0];
  }

  copyTree(index: number) {
    const copiedTree = JSON.parse(JSON.stringify(this.trees[index]));
    const maxId = Math.max.apply(Math, this.trees.map(function(o) { return o.id; }))
    copiedTree.id = String(maxId + 1);
    this.trees.unshift(copiedTree);
    this.tree = this.trees[0];
  }

  deleteTree(index: number) {
    if (this.tree.id === this.trees[index].id) {
      this.selectTree(index + 1);
    }
    this.trees.splice(index, 1);
  }

  importTree(event) {
    this.selectedFiles = event.target.files;
    this.currentFileUpload = this.selectedFiles.item(0);

    this.spinner.show();
    this.uploadService.pushFileToStorage(this.currentFileUpload).subscribe(event => {
      if (event.type === HttpEventType.UploadProgress) {
        this.progress.percentage = Math.round(100 * event.loaded / event.total);
      } else if (event instanceof HttpResponse) {
        this.spinner.hide();
        this.reload();
      }
    });
  }

  exportTrees() {
    const blob = new Blob([JSON.stringify(this.trees, null, "\t")], { type: 'application/octet-stream' });
    this.fileUrl = this.sanitizer.bypassSecurityTrustResourceUrl(window.URL.createObjectURL(blob));
  }

  exportTree(index: number) {
    const blob = new Blob([JSON.stringify([this.trees[index]], null, "\t")], { type: 'application/octet-stream' });
    this.fileUrl = this.sanitizer.bypassSecurityTrustResourceUrl(window.URL.createObjectURL(blob));
  }

  onDrop(event: CdkDragDrop<Tree[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data,
        event.previousIndex,
        event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex, event.currentIndex);
    }
  }

  save() {
    this.startTime = new Date().getTime();
    this.spinner.show();

    this.httpClient.post(environment.apiUrl + '/amts', this.trees).subscribe((result : Tree[])=>{
      this.trees = result;
      this.tree = this.trees[0];
      this.spinner.hide();
    });
  }

  showEvidence(event) {
    window.open(this.commonUtil.kibanaUrl(this.tree.root.label),"_blank");
  }

  updateRendering() {
    //console.log(new Date().getTime());
  }
}
