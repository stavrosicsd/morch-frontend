import { Injectable } from '@angular/core';

@Injectable()
export class AuthService {
  private _loggedIn = false;
  private _role = '';

  constructor() { }

  get loggedIn(): boolean {
    return this._loggedIn;
  }

  set loggedIn(value: boolean) {
    this._loggedIn = value;
  }

  get role(): string {
    return this._role;
  }

  set role(value: string) {
    this._role = value;
  }
}
