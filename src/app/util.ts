import {Injectable} from '@angular/core';
import {environment} from '../environments/environment';

@Injectable()
export class Util {
  kibanaUrl(tag: string): string {
    return environment.kibanaUrl + "/app/kibana#/discover?_g=(refreshInterval:(pause:!t,value:0),time:(from:now-15m,to:now))&_" +
    "a=(columns:!(_source),filters:!(('$state':(store:appState),meta:(alias:!n,disabled:!f," +
    "key:tags,negate:!f,params:(query:morch),type:phrase,value:morch),query:(match:(tags:(query:morch,type:phrase)))),('$state':(store:appState)," +
    "meta:(alias:!n,disabled:!f,key:tags,negate:!f,params:(query:'" + tag + "')," +
    "type:phrase,value:'" + tag + "'),query:(match:(tags:(query:'"  + tag +  "',type:phrase)))))," +
    "interval:auto,query:(language:kuery,query:''),sort:!('@timestamp',desc))";
  }
}
