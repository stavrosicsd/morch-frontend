import {Component, Input, OnInit} from '@angular/core';
import { PanZoomConfig } from 'ng2-panzoom';
import {Tree} from '../model';

@Component({
  selector: 'tree',
  templateUrl: './tree.component.html',
  styleUrls: ['./tree.component.scss']
})
export class TreeComponent implements OnInit {

  @Input() tree: Tree;
  panZoomConfig: PanZoomConfig = new PanZoomConfig;

  constructor() {
    this.panZoomConfig.freeMouseWheel = false;
    this.panZoomConfig.invertMouseWheel = true;
    this.panZoomConfig.scalePerZoomLevel = 1.5;
  }

  ngOnInit() {}
}
