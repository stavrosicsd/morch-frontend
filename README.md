# Description

This repository contains the code and example artifacts related to the paper: **Causality-based Accountability Mechanisms for Socio-Technical Systems**
Please cite the paper when using this tool: 

Amjad Ibrahim, Stavros Kyriakopoulos, Alexander Pretschner, Causality-based Accountability Mechanisms for Socio-Technical Systems, Journal of Responsible Technology, Volumes Will appear soon, 2021.


The backend code is [in this repository](https://bitbucket.org/stavrosicsd/morch/src/master/).

A list of related tools can be found on [this page (under Threat Modeling and Causality)](https://www.in.tum.de/en/i04/software/)

# MorchFrontend

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.0.3.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
