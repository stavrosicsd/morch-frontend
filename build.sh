#!/bin/bash
export DOCKER_HUB_ACCOUNT=stavrostum &&
ng build --prod &&
docker build -t frontend . &&
docker image tag frontend $DOCKER_HUB_ACCOUNT/frontend &&
docker image push $DOCKER_HUB_ACCOUNT/frontend
