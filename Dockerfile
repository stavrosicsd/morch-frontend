FROM nginx:alpine

COPY resources/nginx.conf /etc/nginx/nginx.conf
COPY resources/cert.pem /etc/nginx/ssl/cert.pem
COPY resources/key.pem /etc/nginx/ssl/key.pem

WORKDIR /usr/share/nginx/html

COPY dist/* .
